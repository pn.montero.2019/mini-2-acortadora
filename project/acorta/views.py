from django.shortcuts import render, redirect
from .models import URL
from django.db import IntegrityError

def index(request):
    if request.method == 'POST':
        url = request.POST.get('url')
        short_name = request.POST.get('short_name')

        if not short_name:
            # Generar un nombre corto si no se proporciona uno
            short_name = str(URL.objects.count() + 1)

        try:
            # Intentar obtener la URL original de la base de datos
            url_object, created = URL.objects.get_or_create(short_name=short_name)
        except IntegrityError:
            # Si ocurre una URL duplicada, mostrar un mensaje de error
            error_message = f"El nombre corto '{short_name}' ya está en uso."
            urls = URL.objects.all()
            return render(request, 'acorta/index.html', {'error_message': error_message, 'shortened_urls': urls})

        # Guardar la URL original en la base de datos si es nueva
        if created:
            url_object.original_url = url
            url_object.save()

        # Obtener todas las URLs acortadas
        urls = URL.objects.values_list('short_name', flat=True)
        return render(request, 'acorta/index.html', {'shortened_urls': urls})

    # Obtener todas las URLs acortadas
    urls = URL.objects.values_list('short_name', flat=True)
    return render(request, 'acorta/index.html', {'shortened_urls': urls})

def redirect_to_original(request, short_name):
    url_object = URL.objects.get(short_name=short_name)
    return render(request, 'acorta/redirect.html', {'original_url': url_object.original_url})
