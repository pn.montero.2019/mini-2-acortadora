from django.db import models

# Create your models here.

""" Esta clase representa una tabla en la base de datos 
    que almacenará información sobre las URLs."""

class URL(models.Model):
    original_url = models.URLField(unique=True)
    short_name = models.CharField(max_length=100, blank=True, null=True, unique=True)
    short_url = models.CharField(max_length=100, blank=True, null=True, unique=True)

    def __str__(self):  # Estamos devolviendo el nombre corto si existe, sino el original
        if self.short_name:
            return self.short_name
        else:
            return self.original_url
